﻿using UnityEngine;
using System.Collections;

public class Walking : MonoBehaviour 
	{
	public Rigidbody2D rb;
	//public Animator useAnimator;
	public float gunTime;
	public float swordTime;
	public float punchTime;
	public GameObject munPrefab;
	public GameObject munLaunchPoint;
	public GameObject swordLaunchPoint;
	public GameObject swordPrefab;
	public GameObject punchLaunchPoint;
	public GameObject punchPrefab;
	private bool canFire;
	private bool canSlash;
	private bool canjump;
	private bool canpunch;

	// Use this for initialization
		void Start () 
		{
			rb = GetComponent<Rigidbody2D>();
			canFire = true;
			canSlash = true;
			canjump = true;
			canpunch = true;
		}
	
		// Update is called once per frame
		void Update () 
		{
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				this.transform.Translate (new Vector3 (-8, 0, 0) * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) 
			{
				this.transform.Translate (new Vector3 (-20, 0, 0) * Time.deltaTime);
			}
			else if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) 
			{
				this.transform.Translate (new Vector3 (-20, 0, 0) * Time.deltaTime);
			} 
			
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) 
			{
				this.transform.Translate (new Vector3 (8, 0, 0) * Time.deltaTime);
			} 

			if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift)) 
			{
				this.transform.Translate (new Vector3 (20, 0, 0) * Time.deltaTime);
			} 
			else if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)) 
			{
				this.transform.Translate (new Vector3 (20, 0, 0) * Time.deltaTime);
			} 
			if (Input.GetKeyDown (KeyCode.Space) && canjump ) 
			{
				rb.AddForce(new Vector2(0,400));
				canjump = false;
			}
			if (Input.GetKeyDown(KeyCode.X) && canFire) 
			{
				GameObject munObject=Instantiate(munPrefab,munLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				munObject.name = "Player mun";
				canFire = false;
				StartCoroutine(gunWait());
			} 
			if (Input.GetKeyDown(KeyCode.Z) && canSlash) 
			{
				GameObject swordObject=Instantiate(swordPrefab,swordLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				swordObject.name = "Player sword";
				canSlash = false;
				StartCoroutine(swordWait());
			} 
			if (Input.GetKeyDown (KeyCode.C) && canpunch) 
			{
				this.transform.Translate (new Vector3 (4, 0, 0) * Time.deltaTime);
				GameObject swordObject=Instantiate(punchPrefab,punchLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				swordObject.name = "Player punch";
				canpunch = false;
				StartCoroutine(punchWait());
			} 
		}
		IEnumerator gunWait()
		{
			yield return new WaitForSeconds(gunTime);
			canFire = true;
		}
		IEnumerator swordWait()
		{
			yield return new WaitForSeconds(swordTime);
			canSlash = true;
		}
		IEnumerator punchWait()
		{
			yield return new WaitForSeconds(punchTime);
			canpunch = true;
		}
		void OnCollisionEnter2D(Collision2D other)
		{
			if (other.gameObject.CompareTag ("monsters"))
			{
				//Destroy (this.gameObject, 0);
				
				//Application.LoadLevel (Application.loadedLevelName);
			}
			if (other.gameObject.CompareTag ("DrownStart"))
			{
			Destroy (this.gameObject, 0);

			Application.LoadLevel (Application.loadedLevelName);
			}
			if (other.gameObject.CompareTag ("ground"))
			{
				canjump = true;
			}

		}

}

