﻿using UnityEngine;
using System.Collections;

public class Camera_Script : MonoBehaviour {

	public GameObject mainCamera;
	public GameObject character;
	public Transform charPosi;
	public Transform cameraPosi; 
	private float x,y,z;

	// Use this for initialization
	void Start () {
		
		y = cameraPosi.position.y;
		z = cameraPosi.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		x = charPosi.position.x;
		cameraPosi.position = new Vector3 (x, y, z);
	}
}
