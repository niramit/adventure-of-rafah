﻿using UnityEngine;
using System.Collections;

public class sword : MonoBehaviour {

	public Rigidbody2D rb;
		// Use this for initialization
		void Start () 
		{
			rb = GetComponent<Rigidbody2D>();
			Destroy (this.gameObject, 0.4f);
		}

		// Update is called once per frame
		void Update () {
			transform.Rotate(new Vector3(0,0,-180) * Time.deltaTime, Space.World);
		}

		void OnCollisionEnter2D(Collision2D other)
		{

			if (other.gameObject.CompareTag ("monsters"))
			{
				Destroy (other.gameObject, 0);
				Destroy (this.gameObject, 0.05f);
				// monster die animation
				//GameObject munObject=Instantiate(munPrefab,munLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				//munObject.name = "Player mun";

			}
		}

	}
