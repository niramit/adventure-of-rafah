﻿using UnityEngine;
using System.Collections;

public class Walking : MonoBehaviour 
	{
	public Rigidbody2D rb;
	public Animator useAnimator;
	public float gunTime;
	public float swordTime;
	public GameObject munPrefab;
	public GameObject munLaunchPoint;
	public GameObject swordLaunchPoint;
	public GameObject swordPrefab;
	private bool canFire;
	private bool canSlash;
	// Use this for initialization
		void Start () 
		{
			rb = GetComponent<Rigidbody2D>();
			canFire = true;
			canSlash = true;
		}
	
		// Update is called once per frame
		void Update () 
		{
			if (Input.GetKey (KeyCode.LeftArrow)||Input.GetKey (KeyCode.A)) 
			{
				this.transform.Translate(new Vector3(-10,0,0) * Time.deltaTime);
			} 
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)|| Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) 
			{
				this.transform.Translate(new Vector3(-20,0,0) * Time.deltaTime);
			} 
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) 
			{
				this.transform.Translate (new Vector3 (10, 0, 0) * Time.deltaTime);
				//useAnimator.SetBool ("onMove", true);
			} //else
				{
					//useAnimator.SetBool ("onMove", false);
				}
			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)||Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift) )
			{
				this.transform.Translate(new Vector3(20,0,0) * Time.deltaTime);
			} 
			if (Input.GetKeyDown (KeyCode.Space)) 
			{
				rb.AddForce(new Vector2(0,350));
			} 
			if (Input.GetKeyDown(KeyCode.X) && canFire) 
			{
				GameObject munObject=Instantiate(munPrefab,munLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				munObject.name = "Player mun";
				canFire = false;
				StartCoroutine(gunWait());
			} 
			if (Input.GetKeyDown(KeyCode.Z) && canSlash) 
			{
				GameObject swordObject=Instantiate(swordPrefab,swordLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				swordObject.name = "Player sword";
				canSlash = false;
				StartCoroutine(swordWait());
			} 
		}
		IEnumerator gunWait()
		{
			yield return new WaitForSeconds(gunTime);
			canFire = true;
		}
		IEnumerator swordWait()
		{
			yield return new WaitForSeconds(swordTime);
			canSlash = true;
		}
		void OnCollisionEnter2D(Collision2D other)
		{
			if (other.gameObject.CompareTag ("monsters"))
			{
				Destroy (this.gameObject, 0);
			}
		}
}