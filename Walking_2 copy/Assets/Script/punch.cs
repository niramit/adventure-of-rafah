﻿using UnityEngine;
using System.Collections;

public class punch : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, 0.2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		IMonster mon;
		if (other.gameObject.GetComponent(typeof(IMonster)))
		{
			mon = other.gameObject.GetComponent(typeof(IMonster)) as IMonster;
			mon.hit ();
		}
	}
}
