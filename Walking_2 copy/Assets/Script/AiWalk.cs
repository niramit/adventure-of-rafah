﻿using UnityEngine;
using System.Collections;

public class AiWalk : MonoBehaviour, IMonster
{
	public Rigidbody2D rb;
	private float s;



	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = 10;
		//this.transform.Translate(new Vector3(10,0,0) * Time.deltaTime);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		transform.Translate(new Vector3(s,0,0) * Time.deltaTime);

	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("right") )
		{
			s *= -1;
		}
	}

	public void hit()
	{
		print ("hit");
		Destroy (this.gameObject, 0.0f);
	}
}
