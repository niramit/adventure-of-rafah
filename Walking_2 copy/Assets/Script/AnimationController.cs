﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
	public Animator useAnimator;
	public bool Isright;

	// Use this for initialization
	void Start () 
	{
		Isright = true;
	}
	
	// Update is called once per frame
	void Update () {
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				useAnimator.SetBool ("Iswalkl", true);
				Isright = false;
			} else {
				useAnimator.SetBool ("Iswalkl", false);
				Isright = false;
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
				Isright = false;
			} else if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
				Isright = false;
			} else {
				useAnimator.SetBool ("Isrunl", false);
				Isright = false;
			}

			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				useAnimator.SetBool ("Iswalk", true);
				Isright = true;
			} else {
				useAnimator.SetBool ("Iswalk", false);
				Isright = true;
			}

			if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
				Isright = true;
			} else if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
				Isright = true;
			} else {
				useAnimator.SetBool ("Isrun", false);
				Isright = true;
			}

			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjump", true);
			}

			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			} 
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("ground"))
		{
				this.useAnimator.SetBool ("Isjump", false);
		}
		if (col.gameObject.CompareTag ("ground"))
		{
			this.useAnimator.SetBool ("Isjumpl", false);
		}
	}
}
