﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
	public Animator useAnimator;
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () {
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				useAnimator.SetBool ("Iswalkl", true);
			} else {
				useAnimator.SetBool ("Iswalkl", false);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
			} else if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
			} else {
				useAnimator.SetBool ("Isrunl", false);
			}

			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				useAnimator.SetBool ("Iswalk", true);
			} else {
				useAnimator.SetBool ("Iswalk", false);
			}

			if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
			} else if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
			} else {
				useAnimator.SetBool ("Isrun", false);
			}

			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjump", true);
			}

			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			} 
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
			
			if (Input.GetKeyDown (KeyCode.C)) {
				useAnimator.SetBool ("Ispunch", true);
			} else {
				useAnimator.SetBool ("Ispunch", false);
			}
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("ground"))
		{
				this.useAnimator.SetBool ("Isjump", false);
		}
		if (col.gameObject.CompareTag ("ground"))
		{
			this.useAnimator.SetBool ("Isjumpl", false);
		}
	}
}
