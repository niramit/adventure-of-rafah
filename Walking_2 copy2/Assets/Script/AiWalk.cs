﻿using UnityEngine;
using System.Collections;

public class AiWalk : MonoBehaviour, IMonster
{
	public Rigidbody2D rb;
	private float s;
	public Animator useAnimator;
	private int GhostHealth;



	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = -4;
		GhostHealth = 2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		transform.Translate(new Vector3(s,0,0) * Time.deltaTime);

	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		if (other.gameObject.CompareTag ("Player")) {
			useAnimator.SetBool ("hit", true);
		} else {
			useAnimator.SetBool ("hit", false);
		}
	}

	public void hit()
	{
		GhostHealth--;
		if (GhostHealth == 0) 
		{
			Destroy (this.gameObject, 0.0f);
		}
	}
}
