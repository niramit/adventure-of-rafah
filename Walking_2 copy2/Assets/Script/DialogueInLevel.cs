﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueInLevel : MonoBehaviour 
{
	public GameObject dialog1;
	public GameObject dialog2;
	public GameObject HealthUI;

	private bool IsControlled;

	void Start () 
	{
		dialog1.SetActive (false);
		dialog2.SetActive (false);
		IsControlled = true;
	}

	IEnumerator DialogCountdown()
	{
		IsControlled = false;
		yield return new WaitForSeconds (5.0f);
		dialog1.SetActive (false);
		dialog2.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog2.SetActive (false);
		HealthUI.SetActive (true);
		PauseController.isPause = false;
		Destroy (this.gameObject);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			PauseController.isPause = true;
			dialog1.SetActive (true);
			HealthUI.SetActive (false);
			StartCoroutine (DialogCountdown());
		}
	}
}
