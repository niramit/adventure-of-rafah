﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public GameObject door;
	public GameObject text1;
	public GameObject text2;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			this.transform.Translate (new Vector3 (0, -20, 0) * Time.deltaTime);
			Destroy(door.gameObject, 0.1f);
			Destroy(text1.gameObject, 0.1f);
			Destroy(text2.gameObject, 0.1f);
		}
	}
}
